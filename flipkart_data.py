'Import'
from requests import get 
from bs4 import BeautifulSoup 
import csv

'conv url to html doc'
def Url_Html_Doc(url:str):
    return get(url).text


def bs4_data_get_url(num:int):
    url= f'''https://www.flipkart.com/search?q=android+tv&sid=ckf%2Cczl&as=on&as-show=on&otracker=
            AS_QueryStore_OrganicAutoSuggest_1_3_na_na_ps&otracker1=
            AS_QueryStore_OrganicAutoSuggest_1_3_na_na_ps&as-pos=1&as-type=RECENT&suggestionId=
            android+tv%7CTVs&requestId=09d19841-ad7e-4d6f-b1f7-5d8af784e163&as-backfill=on&page={num}'''
    Soup = BeautifulSoup(Url_Html_Doc(url),'lxml')
    return Soup

def scraping(n:int):
    csv_file= open('flipkart.csv','w')
    csv_writer = csv.writer(csv_file)
    csv_writer.writerow(['Name','Rating','Price','Fecture','Exchange offer','Ratings&Reviews','image'])
    'called fun (bs4 data )'
    scraping_data = bs4_data_get_url(n)
    'Main Div Tage'
    for data in scraping_data.find_all('div',{'class':'_2kHMtA'}):
        fecture = ''
        fevture_data=((data.find_all('li')))
        for i in fevture_data:
            fecture+=str(i.text)+'\n'
        
        try:
            Exchange_off=(data.find('div',{'class':'_3xFhiH'}).text)
            if Exchange_off=='Bank Offer':
                Exchange_off=('')
            else:
                Exchange_off = Exchange_off
        except:
            Exchange_off=('')

        'Img'
        try:
            image = data.find('img',{'class':'_396cs4'})['src']
        except:
            image = 'Error To find image'


        name_of_tv_model ={
            'Name' : data.find('div',class_='_4rR01T').text,
            'Rating':(data.find('div',{'class':'_3LWZlK'}).text),
            'Price':data.find('div',{'class':'_30jeq3 _1_WHN1'}).text,
            'Fecture':fecture, 
            'Exchange_off':Exchange_off , 
            'Total_Review':data.find('span',{'class':'_2_R_DZ'}).text,
            'Off_Offer':data.find('div',{'class':'_3Ay6Sb'}).text,
            'img':image
            }
        csv_writer.writerow([name_of_tv_model['Name'],name_of_tv_model['Rating'],name_of_tv_model['Price'],name_of_tv_model['Fecture'],name_of_tv_model['Exchange_off'],name_of_tv_model['Total_Review'],name_of_tv_model['Off_Offer'],
        name_of_tv_model['img']])


scraping(1)